package personal.layoutinflator;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

public class MainActivity extends AppCompatActivity {
    private Button button;
    private Toast toast;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        this.initComponents();
    }



    private void initComponents() {
        this.button = (Button) this.findViewById(R.id.button);
        this.toast = new Toast(this.getApplicationContext());

        LayoutInflater inflater = (LayoutInflater) getSystemService(LAYOUT_INFLATER_SERVICE);
        View view = inflater.inflate(R.layout.custom_toast_layout, null);

        TextView textView = (TextView) view.findViewById(R.id.custom_toast_text_view);
        textView.setText("Welcome");
        ImageView imageView = (ImageView) view.findViewById(R.id.custom_toast_image_view);
        imageView.setImageResource(R.drawable.arrow);

        this.toast.setView(view);
        this.toast.setDuration(Toast.LENGTH_LONG);

        this.button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                MainActivity.this.toast.show();
            }
        });

    }

}
